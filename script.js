let form = document.forms.register
let inputs = form.querySelectorAll("input");
let ps = form.querySelectorAll('span')
let loader = document.querySelector('.loader_post')

let goods = document.querySelector('.good')
let bads = document.querySelector('.bad')

let btns = document.querySelector('button')

let pattern = {
    name: /^[a-z ,.'-]+$/i,
    surname: /^[a-z ,.'-]+$/i,
    email: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    phone: /^9989[012345789][0-9]{7}$/,
    mother: /^[a-z ,.'-]+$/i,
    father: /^[a-z ,.'-]+$/i,
    age: /^100|[1-9]?\d$/,
    about_me: /^[a-z ,.'-]+$/i,
    js: /^[a-z ,.'-]+$/i,
    html: /^[a-z ,.'-]+$/i,
    css: /^[a-z ,.'-]+$/i,
    car: /^[a-z ,.'-]+$/i,

};

function validate(field, regex) {
    if (regex.test(field.value)) {
        field.classList.add('valid');
        field.classList.remove('invalid');
    } else {
        field.classList.add('invalid');
        field.classList.remove('valid');
    }
}

inputs.forEach((input) => {
    input.onkeyup = () => {
        validate(input, pattern[input.name]);
    };
});

form.onsubmit = (event) => {
    event.preventDefault();
    let arrCheck = []

    let bad = 0
    let good = 0

    btns.onclick = () => {

        bad = 0
        good = 0

        loader.classList.remove('loader_post')
        loader.classList.add('loader')
        setTimeout(() => {
            loader.classList.add('loader_post')
            loader.classList.remove('loader')

            
            inputs.forEach(item => {
                if (item.classList.contains('invalid') || item.value.length === 0) {
                    arrCheck.push('error')
                    btns.classList.remove('btn_good')
                    btns.classList.add('btn_bad')
                    bad++
                    bads.innerHTML = bad
                } else {
                    btns.classList.remove('btn_bad')
                    btns.classList.add('btn_good')
                    good++
                    goods.innerHTML = good
                }
            })

            if (arrCheck.length === 0) {
                submit()
                btns.classList.remove('btn_bad')
                btns.classList.add('btn_good')
            } else {
                btns.classList.remove('btn_good')
                btns.classList.add('btn_bad')
            }

        }, 1000);


    }
};


function submit() {
    let user = {};

    let fm = new FormData(form);

    fm.forEach((value, key) => {
        user[key] = value;
    });

    console.log(user);
}